AddCSLuaFile()

if CLIENT then
	SWEP.BounceWeaponIcon = false
end

SWEP.Author			= "_Dubrovski_"
SWEP.Contact		= "Steam"

SWEP.ViewModelFOV	= 70
SWEP.ViewModelFlip	= false
SWEP.ViewModel 		= "models/weapons/cstrike/c_pist_usp.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_usp.mdl"
SWEP.WorldModel		= ""
SWEP.PrintName		= "Area Placer"
SWEP.Slot			= 0
SWEP.SlotPos		= 1
SWEP.DrawAmmo		= false
SWEP.DrawCrosshair	= true
SWEP.HoldType		= "revolver"
SWEP.Spawnable		= false
SWEP.AdminSpawnable	= false
SWEP.Base			= "weapon_base"
SWEP.UseHands		= true

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Ammo			=  "none"
SWEP.Primary.Automatic		= false

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Ammo			=  "none"
SWEP.Secondary.Automatic	=  false

SWEP.Power = 15

if SWEP.FirstPos == nil then
	SWEP.FirstPos = nil
	SWEP.SecondPos = nil
	SWEP.Mode = 1
	SWEP.NextChange = 0
end

function SWEP:Reload()
	if not IsFirstTimePredicted() then return end
	if self.NextChange > CurTime() then return end
	self.NextChange = CurTime() + 0.25
	
	if self.Mode == 1 then
		self.Mode = 2
		if CLIENT then chat.AddText("Mode 2: Очистить") end
	elseif self.Mode == 2 then
		self.Mode = 3
		if CLIENT then chat.AddText("Mode 3: Добавить") end
		/*
		if CLIENT then
			hook.Add( "ChatText", "drp_roomplacer", function( index, name, text, typ )
				print(typ)
				if LocalPlayer():GetActiveWeapon() == "weapon_drp_roomplacer" then
					if LocalPlayer():GetActiveWeapon().Mode == 1 then
						chat.AddText(typ)
					end
				end
				return false
			end )
		end
		*/
	elseif self.Mode == 3 then
		self.Mode = 4
		if CLIENT then chat.AddText("Mode 4: Moving X") end
	elseif self.Mode == 4 then
		self.Mode = 5
		if CLIENT then chat.AddText("Mode 5: Moving Y") end
	elseif self.Mode == 5 then
		self.Mode = 6
		if CLIENT then chat.AddText("Mode 6: Moving Z") end
	elseif self.Mode == 6 then
		self.Mode = 7
		if CLIENT then chat.AddText("Mode 7: SecMoving X") end
	elseif self.Mode == 7 then
		self.Mode = 8
		if CLIENT then chat.AddText("Mode 8: SecMoving Y") end
	elseif self.Mode == 8 then
		self.Mode = 9
		if CLIENT then chat.AddText("Mode 9: SecMoving Z") end
	elseif self.Mode == 9 then
		self.Mode = 10
		if CLIENT then chat.AddText("Mode 10: DEV") end
	else
		self.Mode = 1
		if CLIENT then chat.AddText("Mode 1: Установка") end
	end
end
function SWEP:Deploy()
end
function SWEP:Initialize()
	self:SetHoldType(self.HoldType)
end
function SWEP:DrawHUD()
	local tr = self.Owner:GetEyeTrace()
	if CLIENT then
		if MAPCONFIG != nil then
			for k,v in pairs(MAPCONFIG.ZONES) do
				for k2,box in pairs(v) do
					cam.Start3D()
						render.SetColorMaterial()
						render.DrawBox( Vector(0,0,0), Angle(0,0,0), box[2], box[3], box[4], true )
					cam.End3D()
					local pos = (box[2] + box[3]) / 2
					//render.DrawSphere( pos, 50, 30, 30, Color( 255, 255, 255, 255 ) )
					pos = pos:ToScreen()
					
					draw.Text( {
						text = box[1],
						font = "BR_Righteous",
						xalign = TEXT_ALIGN_CENTER,
						yalign = TEXT_ALIGN_CENTER,
						pos = { pos.x, pos.y },
					} )
				end
			end
		end
		cam.Start3D()
			render.SetColorMaterial()
			
			local tr2 = util.TraceLine( {
				start = LocalPlayer():GetPos(),
				endpos = LocalPlayer():GetPos() + Angle(90,0,0):Forward() * 10000
			} )
			render.DrawSphere( tr2.HitPos, 1, 30, 30, Color( 255, 0, 255, 100 ) )
			
			render.DrawSphere( tr.HitPos, 1, 30, 30, Color( 255, 255, 255, 100 ) )
			if self.FirstPos != nil then
				render.DrawSphere( self.FirstPos, 1, 30, 30, Color( 0, 255, 0, 255 ) )
				render.DrawSphere( self.FirstPos, 10, 30, 30, Color( 0, 255, 0, 10 ) )
			end
			if self.SecondPos != nil then
				render.DrawSphere( self.SecondPos, 1, 30, 30, Color( 0, 0, 255, 255 ) )
				render.DrawSphere( self.SecondPos, 10, 30, 30, Color( 0, 0, 255, 10 ) )
			end
			if self.FirstPos != nil and self.SecondPos != nil then
				//local wpos = (self.SecondPos - self.FirstPos)
				//local len = (self.SecondPos - self.FirstPos):Length()
				//local trace_c = util.TraceLine( {
				//	start = self.FirstPos,
				//	endpos = self.FirstPos + (self.SecondPos - self.FirstPos):Angle():Forward() * (len / 2),
				//} )
				render.DrawBox( Vector(0,0,0), Angle(0,0,0), self.FirstPos, self.SecondPos, Color(200,0,0, 100), true )
				//self.Owner:SetEyeAngles(wpos)
				//print(wpos)
				//render.DrawSphere( trace_c.HitPos, 15, 30, 30, Color( 255, 0, 0, 15 ) )
				//render.DrawBox( self.FirstPos, Angle(0,0,0), Vector(0,0,0), Vector(10,100,10), Color(255,0,0,100), false )
				//render.DrawLine( Vector startPos, Vector endPos, table color, boolean writeZ=false )
			end
		cam.End3D()
	end
end
function SWEP:Think()
end
function SWEP:PrimaryAttack()
	if self.NextChange > CurTime() then return end
	self.NextChange = CurTime() + 0.25
	if self.Mode == 1 then
		if not IsFirstTimePredicted() then return end
		local tr = self.Owner:GetEyeTrace()
		print(tr.HitPos)
		self.FirstPos = tr.HitPos
		if self.FirstPos != nil and self.SecondPos != nil then
			PrintTable(ents.FindInBox( self.FirstPos, self.SecondPos ))
		end
	elseif self.Mode == 2 then
		self.FirstPos = nil
		self.SecondPos = nil
		if CLIENT then chat.AddText("Cleaned") end
	elseif self.Mode == 3 then
		if CLIENT then
			local f = self.FirstPos
			local s = self.SecondPos
			local str = 'pos1 = Vector('..math.Round(f.x)..","..math.Round(f.y)..","..math.Round(f.z).."),\n		pos2 = Vector("..math.Round(s.x)..","..math.Round(s.y)..","..math.Round(s.z).."),"
			print(str)
			chat.AddText(str)
		end
	elseif self.Mode == 4 then
		self.FirstPos = self.FirstPos - Vector(self.Power,0,0)
	elseif self.Mode == 5 then
		self.FirstPos = self.FirstPos - Vector(0,self.Power,0)
	elseif self.Mode == 6 then
		self.FirstPos = self.FirstPos - Vector(0,0,self.Power)
		
	elseif self.Mode == 7 then
		self.SecondPos = self.SecondPos - Vector(self.Power,0,0)
	elseif self.Mode == 8 then
		self.SecondPos = self.SecondPos - Vector(0,self.Power,0)
	elseif self.Mode == 9 then
		self.SecondPos = self.SecondPos - Vector(0,0,self.Power)
	elseif self.Mode == 9 then
		--[[ local tr = self.Owner:GetEyeTrace()
		print(tr:GetModel()) ]]
		for k,v in pairs(ents.FindByModel("models/props_phx/misc/traffic_light_uk_single_b_rashk.mdl")) do
			print("1")
		end
	end
end
function SWEP:SecondaryAttack()
	if not IsFirstTimePredicted() then return end
	if self.Mode == 1 then
		local tr = self.Owner:GetEyeTrace()
		print(tr.HitPos)
		self.SecondPos = tr.HitPos
	elseif self.Mode == 4 then
		self.FirstPos = self.FirstPos + Vector(self.Power,0,0)
	elseif self.Mode == 5 then
		self.FirstPos = self.FirstPos + Vector(0,self.Power,0)
	elseif self.Mode == 6 then
		self.FirstPos = self.FirstPos + Vector(0,0,self.Power)
		
	elseif self.Mode == 7 then
		self.SecondPos = self.SecondPos + Vector(self.Power,0,0)
	elseif self.Mode == 8 then
		self.SecondPos = self.SecondPos + Vector(0,self.Power,0)
	elseif self.Mode == 9 then
		self.SecondPos = self.SecondPos + Vector(0,0,self.Power)
	end
end


