﻿AddCSLuaFile()
print("Dubrovski Transport System (special for Rashkinsk) - Config loaded") -- Можно убрать при заливе на основу. Нужно чисто для теста

TRANSPORT_CONFIG = {}

/*
	█▀▀▄ █░░█ █▀▀▄ █▀▀█ █▀▀█ ▀█░█▀ █▀▀ █░█ ░▀░   ▀▀█▀▀ █▀▀█ █▀▀█ █▀▀▄ █▀▀ █▀▀█ █▀▀█ █▀▀█ ▀▀█▀▀   █▀▀ █░░█ █▀▀ ▀▀█▀▀ █▀▀ █▀▄▀█
	█░░█ █░░█ █▀▀▄ █▄▄▀ █░░█ ░█▄█░ ▀▀█ █▀▄ ▀█▀   ░░█░░ █▄▄▀ █▄▄█ █░░█ ▀▀█ █░░█ █░░█ █▄▄▀ ░░█░░   ▀▀█ █▄▄█ ▀▀█ ░░█░░ █▀▀ █░▀░█
	▀▀▀░ ░▀▀▀ ▀▀▀░ ▀░▀▀ ▀▀▀▀ ░░▀░░ ▀▀▀ ▀░▀ ▀▀▀   ░░▀░░ ▀░▀▀ ▀░░▀ ▀░░▀ ▀▀▀ █▀▀▀ ▀▀▀▀ ▀░▀▀ ░░▀░░   ▀▀▀ ▄▄▄█ ▀▀▀ ░░▀░░ ▀▀▀ ▀░░░▀
*/
-- Профессия водителя автобуса
TRANSPORT_CONFIG.BusJob = TEAM_BUS

-- Профессия машиниста трамвая
TRANSPORT_CONFIG.TramJob = TEAM_TRAM


-- Зарплата, которую получает машинист за каждую остановку
TRANSPORT_CONFIG.Salary_Stop = 50

-- Надбавка за каждого пассажира в транпорте
TRANSPORT_CONFIG.Salary_Passenger = 20

-- Зарплата за пройденный круг = прибытие на станцию Городской Парк
TRANSPORT_CONFIG.Salary_Finish = 500

-- Время в секундах, которое должен подождать водитель прежде чем ему засчитается остановка
TRANSPORT_CONFIG.Stop_timer = 5

-- Радиус оповещения о прибытии транспорта около остановки
TRANSPORT_CONFIG.Info_radius = 1000
-- Время, которое это оповещение будет висеть (секунды)
TRANSPORT_CONFIG.Info_time = 15

-- Сноски на конфиг сломанного аддона на билеты (Временно)
TRANSPORT_CONFIG.ConfigurableVehicles = {
	["models/dk_cars/rashkinsk/liaz/677.mdl"] = true,
	["models/cmbfdr/vehicles/paz3205.mdl"] = true,
	["models/pantera_71/gaz322132/gaz322132.mdl"] = true,
	["models/cmbfdr/vehicles/liaz_5256.mdl"] = true,
}

-- Остановки на картах можно настроить в папке mapconfigs. Каждой карте соответствует файл с соответствующим названием
-- Ниже можно уже ничего не трогать. Конфиг кончается здесь.

/*
	█▀▀ █▀▀▄ █▀▀▄   █▀▀█ █▀▀   █▀▀ █▀▀█ █▀▀▄ █▀▀ ░▀░ █▀▀▀
	█▀▀ █░░█ █░░█   █░░█ █▀▀   █░░ █░░█ █░░█ █▀▀ ▀█▀ █░▀█
	▀▀▀ ▀░░▀ ▀▀▀░   ▀▀▀▀ ▀░░   ▀▀▀ ▀▀▀▀ ▀░░▀ ▀░░ ▀▀▀ ▀▀▀▀
*/

TRANSPORT_CONFIG.CustomFuncOfConfigurable = function(ent)
	if IsValid(ent) and IsValid(ent:GetParent()) and IsValid(ent.Tram) and ent == ent.Tram.DriverSeat then
		return true
	end
	return false
end

