﻿--AddCSLuaFile()
include( "autorun/config.lua" )
mapfile = "mapconfigs/" .. game.GetMap() .. ".lua"
AddCSLuaFile(mapfile)
include(mapfile)
print("Dubr - Client Run")

surface.CreateFont( "HUDFontLarge", {
	font = "Bauhaus",    
	size = 25,
	weight = 700,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
})

surface.CreateFont( "HUDFontMedium", {
	font = "Bauhaus",    
	size = 21,
	weight = 700,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
})

	local stoptimer = TRANSPORT_CONFIG.Stop_timer
	
local function IsPassengerSeat(seat)
if IsValid(seat) and seat:IsVehicle() then
	if IsValid(seat:GetParent()) and seat:GetParent():GetNWInt("TransPayPrice", 0) > 0 and seat:GetNWInt("SeatType", -1) == 1 then
		return true
	end
end

return false
end

local function IsConfigurableVeh(seat)
	if IsValid(seat) and seat:IsVehicle() then
		if TRANSPORT_CONFIG.ConfigurableVehicles[seat:GetModel():lower()] or TRANSPORT_CONFIG.CustomFuncOfConfigurable(seat) then
			return true
		end
	end
	return false
end


local function IsInVehicle(ply)
	if ply:InVehicle() and IsConfigurableVeh(ply:GetVehicle()) and (!CPPI or ply:GetVehicle():CPPIGetOwner() == ply) and !IsPassengerSeat(seat) then
		return true
	elseif ply:GetVehicle():GetNWInt("SeatType", -1) == 0 then
		return true
	else
		return false	
	end
end

local function DrawInfoStation(pos, txt, clr)
	pos = pos:ToScreen()
	draw.TextShadow( {
		text = txt,
		pos = { pos.x + 1, pos.y - 1},
		font = "HUDFontMedium",
		color = clr,
		xalign = TEXT_ALIGN_CENTER,
		yalign = TEXT_ALIGN_CENTER,
	}, 2, 255 )
end



hook.Add( "HUDPaint", "BusDriver_HUD", function()

	if LocalPlayer():Team() == TEAM_BUS or LocalPlayer():Team() == TEAM_TRAM then

		local nextstop = Bus_stations[LocalPlayer():GetNWInt("Transport_NextStation", 1)]
		local firststop = Bus_stations[1]
		if LocalPlayer():Team() == TEAM_BUS then
			nextstop = Bus_stations[LocalPlayer():GetNWInt("Transport_NextStation", 1)]
--[[ 			firststop = Bus_stations[1]
			if LocalPlayer():GetNWInt("Transport_NextStation", 1) > Bus_numstations then
				nextstop = firststop
			end ]]
		elseif LocalPlayer():Team() == TEAM_TRAM then
			nextstop = Tram_stations[LocalPlayer():GetNWInt("Transport_NextStation", 1)]
--[[ 			firststop = Tram_stations[1]
			if LocalPlayer():GetNWInt("Transport_NextStation", 1) > Tram_numstations then
				nextstop = firststop
			end ]]
		end
		
		
		if LocalPlayer():GetPos():WithinAABox( nextstop.pos1, nextstop.pos2 ) and IsInVehicle(LocalPlayer()) then
			draw.SimpleTextOutlined(
				"Вы подъехали к остановке: "..nextstop.name,
					"HUDFontLarge",
					ScrW()*0.5,
					ScrH()*0.1,
					Color(255,255,255),
					TEXT_ALIGN_CENTER,
					TEXT_ALIGN_RIGHT,
					1,
					Color( 0, 0, 0, 255 )
			)
			
			if !LocalPlayer():GetNWBool("Transport_Embarking", false) then
				draw.SimpleTextOutlined(
					"Нажмите ПРОБЕЛ для начала посадки/высадки",
						"HUDFontLarge",
						ScrW()*0.5, 
						ScrH()*0.15,
						Color(255,255,0),
						TEXT_ALIGN_CENTER,
						TEXT_ALIGN_RIGHT,
						1,
						Color( 0, 0, 0, 255 )
				)
				render.SetColorMaterial()
				cam.Start3D()
					render.DrawBox( Vector(0,0,0), Angle(0,0,0), Vector(nextstop.pos1), Vector(nextstop.pos2), Color(0,200,0, 30), true )
				cam.End3D()
			end
		elseif LocalPlayer():InVehicle() and IsInVehicle(LocalPlayer()) then
			draw.SimpleTextOutlined(
				"Направляйтесь на остановку "..nextstop.name.." ("..math.Round(LocalPlayer():GetPos():Distance(nextstop.pos1)/100).." м)",
				"HUDFontLarge",
				ScrW()*0.5, 
				ScrH()*0.1,
				Color(0,200,0),
				TEXT_ALIGN_CENTER,
				TEXT_ALIGN_RIGHT,
				1,
				Color( 0, 0, 0, 255 )
			)
			render.SetColorMaterial()
			cam.Start3D()
				render.DrawBox( Vector(0,0,0), Angle(0,0,0), Vector(nextstop.pos1), Vector(nextstop.pos2), Color(200,0,0, 30), true )
			cam.End3D()
			if LocalPlayer():GetPos():Distance(nextstop.pos1)/100 > 30 then
				DrawInfoStation((((nextstop.pos2-nextstop.pos1)/2) + nextstop.pos1), nextstop.name.." ("..math.Round(LocalPlayer():GetPos():Distance(nextstop.pos1)/100).." м)", Color(0, 200, 0))
			end
		else
			draw.SimpleTextOutlined(
				"Вы должны находиться в транспорте для выполнения работы",
				"HUDFontLarge",
				ScrW()*0.5, 
				ScrH()*0.1,
				Color(255,0,0),
				TEXT_ALIGN_CENTER,
				TEXT_ALIGN_RIGHT,
				1,
				Color( 0, 0, 0, 255 )
			)
		end
	end		
end)