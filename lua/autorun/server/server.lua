﻿include( "autorun/config.lua" )

mapfile = "mapconfigs/" .. game.GetMap() .. ".lua"
AddCSLuaFile(mapfile)
include(mapfile)

for key, value in pairs(Bus_stations) do
    local defaults = {
        time = TRANSPORT_CONFIG.Stop_timer
    }
    
    table.Inherit(value, defaults)
    Bus_stations[key] = value
end

if CLIENT then return false end

-- Внутренная функция для будущих скриптов и тестирования. Возвращает игрока по его нику
function GetPlayer(nick)
	for k,v in pairs(player.GetAll()) do
		if v:Nick() == nick then
			return v
		end
	end
	return nil
end
-- Для трамваев - проверка что машинист сидит на водительском кресле. Если вернёт true - значит на пассажирском
local function IsPassengerSeat(seat)
	if IsValid(seat) and seat:IsVehicle() then
		if IsValid(seat:GetParent()) and seat:GetParent():GetNWInt("TransPayPrice", 0) > 0 and seat:GetNWInt("SeatType", -1) == 1 then
			return true
		end
	end
	
	return false
end
-- Проверка на то, что этот тип транспорта действительно является общественным из конфига
local function IsConfigurableVeh(seat, ply)
	if IsValid(seat) and seat:IsVehicle() then
		if TRANSPORT_CONFIG.ConfigurableVehicles[seat:GetModel():lower()] or TRANSPORT_CONFIG.CustomFuncOfConfigurable(seat) then
			return true
		end
	end
	return false
end
-- Общая проверка на соответствие транспорта, в котором сидит игрок. Используется везде.
function IsInVehicle(ply)
	if ply:InVehicle() and IsConfigurableVeh(ply:GetVehicle()) and (!CPPI or ply:GetVehicle():CPPIGetOwner() == ply) then
		return true
	elseif ply:GetVehicle():GetNWInt("SeatType", -1) == 0 then
		return true
	else
		return false
	end
end

-- Открытие дверей (для трамвая)
local function OpenDoors(ply)
	--[[ local seat = ply:GetVehicle()
	if !TRANSPORT_CONFIG.CustomFuncOfConfigurable(seat) then return false end
	if IsValid(seat) then
		local tram = seat:GetNWEntity("TrainEntity")
		if !tram.DriverDoorOpened then
			tram:ToggleDoors(false)
			tram:SetNWInt("Controller", -4)
		end
		if !tram.DoorsOpened then
			tram:ToggleDoors(true)
			tram:SetNWInt("Controller", -4)
		end
	end ]]
end
-- Закрытие дверей (для трамвая)
local function CloseDoors(ply)
	local seat = ply:GetVehicle()
	if !TRANSPORT_CONFIG.CustomFuncOfConfigurable(seat) then return false end
	if IsValid(seat) then
		local tram = seat:GetNWEntity("TrainEntity")
		if tram.DriverDoorOpened then
			timer.Simple(4, function() tram:ToggleDoors(false) tram:SetNWInt("Controller", 0) end)
			tram:SetNWInt("Controller", -4)
			timer.Simple(1, function() tram:SetNWInt("Controller", -4) end)
			timer.Simple(2, function() tram:SetNWInt("Controller", -4) end)
			timer.Simple(3, function() tram:SetNWInt("Controller", -4) end)
		end
		if tram.DoorsOpened then
			tram:ToggleDoors(true)
			tram:SetNWInt("Controller", -4)
			timer.Simple(4, function() tram:ToggleDoors(false) tram:SetNWInt("Controller", 0) end)
		end
	end
end
-- Уведомление после прибытия на остановку
function Waypoint_Notify(ply)
	local stationid = ply:GetNWInt("Transport_NextStation", 1)
	local nextstop = Bus_stations[stationid]
	local nextstop1 = Bus_stations[stationid+1]
	local transport_type = "Транспорт"
	if ply:Team() == TEAM_BUS then
		nextstop = Bus_stations[stationid]
		nextstop1 = Bus_stations[stationid+1]
		transport_type = "Автобус"
	elseif ply:Team() == TEAM_TRAM then
		nextstop = Tram_stations[stationid]
		nextstop1 = Tram_stations[stationid+1]
		transport_type = "Трамвай"
	end
	
	if ply:Team() == TEAM_BUS or ply:Team() == TEAM_TRAM then
		for k,v in pairs(player.GetAll()) do
			if v:GetPos():Distance(ply:GetPos()) <= TRANSPORT_CONFIG.Info_radius then
				DarkRP.notify(v, NOTIFY_HINT, nextstop.time, transport_type.." отправится к "..nextstop1.name.." через "..nextstop.time.." сек")
				DarkRP.notify(v, NOTIFY_HINT, nextstop.time, transport_type.." прибыл на остановку "..nextstop.name)
			end
		end
	end
end
-- По прибытии на остановку и нажатию кнопки ПРОБЕЛ
function Waypoint_Arrive(ply)
	if !IsInVehicle(ply) then
		DarkRP.notify(ply, NOTIFY_ERROR, 5, "Вы должны находиться в транспорте, чтобы работать")
		return false
	end
	local stationid = ply:GetNWInt("Transport_NextStation", 1)
	local nextstop = Bus_stations[stationid]
	local nextstop1 = Bus_stations[stationid+1]
	local firststop = Bus_stations[1]
	local Max_numstations = 100
	local passengers = table.Count(ply:GetVehicle():CMGetRealPassengers())-1
	
	if ply:Team() == TEAM_BUS then
		nextstop = Bus_stations[stationid]
		nextstop1 = Bus_stations[stationid+1]
		firststop = Bus_stations[1]
		Max_numstations = Bus_numstations
	elseif ply:Team() == TEAM_TRAM then
		nextstop = Tram_stations[stationid]
		nextstop1 = Tram_stations[stationid+1]
		firststop = Tram_stations[1]
		Max_numstations = Tram_numstations
	end
	
		if ply:GetNWInt("Transport_NextStation", 1) > Bus_numstations then
			nextstop = firststop
			nextstop1 = firststop
		end
		if ply:GetPos():WithinAABox(nextstop.pos1, nextstop.pos2) then
			DarkRP.notify(ply, NOTIFY_HINT, nextstop.time + 1, "Вы прибыли на остановку "..nextstop.name..". Подождите "..nextstop.time.." секунд.")
			if stationid < Max_numstations then
				Waypoint_Notify(ply)
			end
			ply:SetNWBool("Transport_Embarking", true)
			OpenDoors(ply)
			timer.Create(ply:SteamID64().."_BusStop", nextstop.time, 1, function()
					CloseDoors(ply)
					if ply:GetPos():WithinAABox(nextstop.pos1, nextstop.pos2) then
						if stationid < Max_numstations then
							ply:SetNWInt("Transport_NextStation", stationid+1)
							ply:SetNWBool("Transport_Embarking", false)
							if passengers > 0 then
								DarkRP.notify(ply, NOTIFY_HINT, 5, "Вы получили "..TRANSPORT_CONFIG.Salary_Stop.." руб. за остановку. Отправляйтесь на: "..nextstop1.name)
								DarkRP.notify(ply, NOTIFY_HINT, 5, "За пассажиров ("..passengers..") вы получили надбавку: "..TRANSPORT_CONFIG.Salary_Passenger * passengers)
								ply:addMoney(TRANSPORT_CONFIG.Salary_Stop + TRANSPORT_CONFIG.Salary_Passenger * passengers)
							else
								DarkRP.notify(ply, NOTIFY_HINT, 5, "Вы получили "..TRANSPORT_CONFIG.Salary_Stop.." руб. за остановку. Отправляйтесь на: "..nextstop1.name)
								ply:addMoney(TRANSPORT_CONFIG.Salary_Stop)
							end
						else
							DarkRP.notify(ply, NOTIFY_HINT, 5, "Вы получили бонусные "..TRANSPORT_CONFIG.Salary_Finish.." рублей за прохождение маршрута.")
							ply:SetNWInt("Transport_NextStation", 1)
							ply:SetNWBool("Transport_Embarking", false)
							ply:addMoney(TRANSPORT_CONFIG.Salary_Finish)
							DarkRP.notify(ply, NOTIFY_HINT, 5, "Отправляйтесь на: "..firststop.name)
						end
					else
						DarkRP.notify(ply, NOTIFY_ERROR, 5, "Вы уехали с остановки. Вернитесь к "..nextstop.name)
						ply:SetNWBool("Transport_Embarking", false)
					end
				end )
		end
end

hook.Add( "KeyPress", "Dubr_Transport_Press", function( ply, key )
	if ( key == IN_JUMP ) and IsInVehicle(ply) and !timer.Exists(ply:SteamID64().."_BusStop") then
		Waypoint_Arrive(ply)
	end
end )